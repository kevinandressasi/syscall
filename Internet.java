/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.kass.sistemasoperativos;

import java.io.*;
import java.net.*;

public class Internet {
    static int n;
    static long[] arr;
    static long maximo;
    public static String conexion(String maquina) throws UnknownHostException, IOException {
        if (InetAddress.getByName(maquina).isReachable(5000)) {
            return "Conectado a internet";
        }
        return "Sin conexión a internet";
    }

    public static void pings(int nn) throws IOException, InterruptedException {
        n = nn;
       arr = new long[n];
        for (int i = 0; i < n; i++) {
            long start = System.currentTimeMillis();
            Process process = Runtime.getRuntime().exec("ping -c 1 goole.com");
            process.waitFor();
            long end = System.currentTimeMillis();
            arr[i] = end - start;
        }
         maximo = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Ping " + (i + 1) + ": " + arr[i] + "ms");
            if (arr[i] > maximo) {
                maximo = arr[i];
            }
        }
        System.out.println("Ping con mayor demora: " + maximo + "ms");
    }
    
    public static void mostrarTxt() throws IOException{
        File file = new File("ping.txt");
        FileWriter writer = new FileWriter(file);
        writer.write("Resultados de ping:\n");
        for (int i = 0; i < n; i++) {
            writer.write("Ping " + (i + 1) + ": " + arr[i] + "ms\n");
        }
        writer.write("Ping con mayor demora: " + maximo + "ms\n");
        writer.close();

        // Abrir el archivo en el bloc de notas
        ProcessBuilder pb = new ProcessBuilder("Notepad.exe", file.getAbsolutePath());
        pb.start();
    }
    
    

    public static void main(String[] args) throws IOException, InterruptedException {
        // Comprobar la conectividad a internet
        String maquina = "goole.com";
        System.out.println(conexion(maquina));
        pings(10);
        mostrarTxt();
    }
}
