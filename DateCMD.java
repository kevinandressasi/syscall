/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.kass.sistemasoperativos;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 *
 * @author kevin
 */
public class DateCMD {

    public static void main(String[] args) {

        try {
            Process proceso = Runtime.getRuntime().exec("cmd /c date /t"); // Ejecuta el comando "date /t" en una nueva ventana del cmd
            InputStream entrada = proceso.getInputStream(); // Captura la salida del proceso
            InputStreamReader isr = new InputStreamReader(entrada);
            BufferedReader br = new BufferedReader(isr);
            String linea;
            while ((linea = br.readLine()) != null) { // Lee la salida del proceso
                System.out.println(linea);
                String nombreArchivo = "fecha.txt";
                PrintWriter escritor = new PrintWriter(new FileWriter(nombreArchivo));
                escritor.println(linea);
                escritor.close();
                File archivo = new File(nombreArchivo);
                Desktop.getDesktop().open(archivo);
               
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
