/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.kass.sistemasoperativos;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 *
 * @author kevin
 */
public class TreeCMD {

    public static void main(String[] args) throws InterruptedException {

        try {
            Process proceso = Runtime.getRuntime().exec("cmd /c tree C:\\"); // Ejecuta el comando "tree C:\" en una nueva ventana del cmd
            InputStream entrada = proceso.getInputStream();
            System.out.println(proceso.waitFor());
            InputStreamReader isr = new InputStreamReader(entrada);
            BufferedReader br = new BufferedReader(isr);
            String linea;
            while ((linea = br.readLine()) != null) {
                String nombreArchivo = "arbol.txt";
                PrintWriter escritor = new PrintWriter(new FileWriter(nombreArchivo, true)); // true indica que el archivo se abre en modo append
                escritor.println(linea);
                escritor.close();
            }
            File archivo = new File("arbol.txt");
            Desktop.getDesktop().open(archivo);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
